# Veracode-helpers  
  
The Veracode helpers part is based 2 external Veracode Community projects.  
- [VeracodeSASTResultsImport](https://gitlab.com/julz0815/veracodesastresultsimport)  
- [SCAResultsReport](https://gitlab.com/julz0815/scaresultsreport)  
  
## Helpers to work with  Veracode static scan results  
Folder Veracode-Static-Results, documentation on this folder.  
`VeracodeSASTResultsImport.js`  
  
  
## Helpers to work with Veraocde SCA Agent Based results  
Folder Veracode-SCA-Results, documentation on this folder.  
`dependecies.js`  