# Veracaode - Gitlab Static results report and issue generation 

## About

this little Java Script will download json results from a Veracode policy or sandbox scan into Gitlab readable report format in order display results as SAST results on the pipeline run and create Gitlab issues on the findings
 
## Instructions

- Copy the VeracodeSASTResultsImport.js and package.json file into your rpository
- set up a job to run Veracode static analysis (policy or sandbox scan)
- run the VeracodeSASTResultsImport.js script using following task


## Command arguments issue creation
The script takes up to 10 command line arguments to achieve different things  

1 - required    - scan_type         = policy|sandobx  
2 - required    - porfile_name      = PROFILE_NAME  
3 - optional    - app_guid          = EMPTY|APP_GUID  
4 - optional    - scan_guid         = EMPTY|SCAN_GUID  
5 - optional    - sandbox_name      = SANDBOX_NAME  
6 - optional    - sandbox_guid      = SANDBOX_GUID  
7 - required    - gitlab_token      = GITLAB PRIVATE TOKEN  
8 - required    - create_issue      = true|false  
9 - required    - gitlab_project    = GITLAB PROJECT IP   


If you only want to display results on the security tab of your pipeline you only need to run the script with a feww arguments  
All required ones and make sure you set create_issues=false  
  
If you want to also create issues from findings there are a few more arguments you need to set, mandatory one for that is for sure create_issue=true  
Duplication checks are done for the last 100 exisiting issues for now. If the issue is not part of these 100 exisitng issues, it will be recreated.  
If overwrite_issues is set to true it allows you to create a new issue if there is already a open/closed Gitlab issue, but the findings is still showing up on a newer scan  
  
  
## General Settings  
Root folder settings for Java applications  
Select Settings > CI/CD > Variables.  
Set these environment variables:  
SRC_ROOT: the filepath typically is /src/main/java/.  
JSP_ROOT: the filepath typically is /src/main/webapp/.   
  

Private Token  
Select User setting > Access tokens > scopes > api  
Store the token securely and rememeber to set an environment variable:  
Select Settings > CI/CD > Variables  
Set PRIVATE_TOKEN to the corresponding value and mark as masked  
  
  
Veracode Credentials  
Select Settings > CI/CD > Variables.  
Set these environment variables:  
API_ID: your Veracode API ID from the Veracode Platform  
API_KEY: your Veracode API Secret Key from the Veracode Platform  
  
  
  
## Security report creation

The Java Script will take the application profile name, and if a sandbox scan the sandbox name, (correspondigly the app_guid or sandbox_guid) as parameters to download the scan results from the last scan using the findings API.
The Java Script output is the actual report read by Gitlab and need to be set as report artifact as well.

The below example is split into 2 tasks, one is running the static analyssi as a policy scan using the Veracode API wrapper docker and Veracode API wrapper. It's also configured to allow_failure: true in order to run the 2nd task and actually download the findigns from this finished policy/sandbox scan.  
  

## Example job


```
Policy Scan Static Analysis:
    image: veracode/api-wrapper-java
    stage: Security_Scan
    only:
        - schedules
        - master
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid ${API_ID} -vkey ${API_KEY}
          -action UploadAndScan -appname "Verademo" -createprofile false -autoscan true
          -filepath ./target/verademo.war -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" 
          -scantimeout 15 2>&1 | tee policy_scan_output.txt
    artifacts:
        paths:
            - policy_scan_output.txt
        when: always
        name: "veracode-POLICY-SCAN-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true

Generate static analysis report and issues:
    image: node:latest
    stage: Reporting
    only:
        - master
    before_script:
        - npm ci
    script:
        - npm run results-import scan_type=policy profile_name=Verademo gitlab_token=${PRIVATE_TOKEN} gitlab_project=25491455 create_issue=false  
    artifacts:
        reports:
            sast: output-sast-vulnerabilites.json
        paths: 
            - output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
```

## Example output on your pipeline security tab
![](Screenshot.png)


## Issue creation

The Java Script will take the application profile name, and if a sandbox scan the sandbox name, (correspondigly the app_guid or sandbox_guid) as parameters to download the scan results from the last scan using the findings API.
All open findings (up to 500) will create individual issues.  
If the flaw is still seen as NEW, OPEN or REOPENED on the Veracode scan but the Gitlab issue is already closed, a new Gitlab issue will be created.  

The below example is split into 2 tasks, one is running the static analyssi as a policy scan using the Veracode API wrapper docker and Veracode API wrapper. It's also configured to allow_failure: true in order to run the 2nd task and actually download the findigns from this finished policy/sandbox scan.  
  
  
## Example job


```
Policy Scan Static Analysis:
    image: veracode/api-wrapper-java
    stage: Security_Scan
    only:
        - schedules
        - master
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid ${API_ID} -vkey ${API_KEY}
          -action UploadAndScan -appname "Verademo" -createprofile false -autoscan true
          -filepath ./target/verademo.war -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" 
          -scantimeout 15 2>&1 | tee policy_scan_output.txt
    artifacts:
        paths:
            - policy_scan_output.txt
        when: always
        name: "veracode-POLICY-SCAN-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true

Generate static analysis report and issues:
    image: node:latest
    stage: Reporting
    only:
        - master
    before_script:
        - npm ci
    script:
        - npm run results-import scan_type=policy profile_name=Verademo gitlab_token=${PRIVATE_TOKEN} gitlab_project=25491455 create_issue=true  
    artifacts:
        reports:
            sast: output-sast-vulnerabilites.json
        paths: 
            - output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
```

## Example output on your issue list
Issues list
![](Issues1.png)

Issues details
![](Issues2.png)



